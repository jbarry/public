// "use strict"

export class BitString {
  constructor(bits) {
    this.update(this.compress(bits));
  }

  [Symbol.iterator]() {
    this.index = 0;
    return {
      next: () => {
        if (this.index < this.decompress().length) {
          return {value: this.get_item(this.index++), done: false};
        } else {
          this.index = 0; // If you want to iterate over this again without forcing manual update of the index
          return {done: true};
        }
      }
    }
  }

  update(input) {
    if (input < 1) {
      console.error("value error");
    } else {
      this.stored = input;
    }
  }

  compress(bits) {
    let sentinel = "1" + bits; // sentinel allows the bit string to retain any leading 0s
    return parseInt(sentinel, 2);
  }

  decompress() {
    return this.stored.toString(2).slice(1); // slice removes the first character of the string, which was added by the sentinel
  }

  value() {
    return this.stored - 2 ** this.decompress(this.stored).length; // removes whatever was added by the sentinel
  }

  get_item(key) {
    return this.decompress(this.stored)[key];
  }
}
