// "use strict"

import { BitString } from './BitString.js';

export class CompressedGene {
  constructor(gene) {
    this.compress(gene);
  }

  compress(gene) {
    this.bit_gene = new BitString("")
    for (const nucleotide of gene) {
      this.bit_gene.update(this.bit_gene.stored << 2);
      if (nucleotide === "A") {
        this.bit_gene.update(this.bit_gene.stored | 0b00);
      } else if (nucleotide === "C") {
        this.bit_gene.update(this.bit_gene.stored | 0b01);
      } else if (nucleotide === "G") {
        this.bit_gene.update(this.bit_gene.stored | 0b10);
      } else if (nucleotide === "T") {
        this.bit_gene.update(this.bit_gene.stored | 0b11);
      } else {
        console.error("value error");
      }
    }
  }

  decompress() {
    this.gene = "";
    for (let i = 0; i < this.bit_gene.decompress().length; i += 2) {
      let bit_section = "";
      bit_section += this.bit_gene.get_item(i);
      bit_section += this.bit_gene.get_item(i + 1);

      if (bit_section === "00") {
        this.gene += "A";
      } else if (bit_section === "01") {
        this.gene += "C";
      } else if (bit_section === "10") {
        this.gene += "G";
      } else if (bit_section === "11") {
        this.gene += "T";
      } else {
        console.error("decompress value error");
      }
    }
    return this.gene;
  }
}