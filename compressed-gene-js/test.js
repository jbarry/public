// let bits = "010";

// for (let i in bits) {
//   console.log(bits.charAt(i))
// }

// let split_bits = bits.split('');

// for (let i in split_bits) {
//   console.log(split_bits[i])
// }

// class BitString {
//   constructor(bits) {
//     this.stored = this.compress(bits);
//     if (this.stored < 1)
//     {
//       throw new Error(`value error`);
//     }
//   }

//   compress(bits) {
//     let sentinel = 1;
//     for (let i in bits) {
//       sentinel <<= 1;
//       if (bits[i] === "0") {
//         sentinel |= 0b0;
//       } else if (bits[i] === "1") {
//         sentinel |= 0b1;
//       } else {
//         console.error("value error");
//       }
//     }
//     return sentinel;
//   }
// }

// const x = new BitString("0111");
// x.stored -= 24;

// console.log(x.stored);


// class BitString {
//   constructor(bits) {
//     this.compress(bits);
//   }

//   compress(bits) {
//     let sentinel = 1; // sentinel allows the bit string to retain any leading '0's
//     for (let i in bits) {
//       sentinel <<= 1;
//       if (bits[i] === "0") {
//         sentinel |= 0b0;
//       } else if (bits[i] === "1") {
//         sentinel |= 0b1;
//       } else {
//         console.error("value error");
//       }
//     }

//     if (sentinel >= 1) this.stored = sentinel; // A bit string should never represent a negative number, and in this case should never be less than 1 because it includes the sentinel
//     else console.error("value error");
//   }

//   decompress(input) {
//     return this.stored.toString(2).slice(1); // slice removes the first character of the string, which is the sentinel
//   }

//   restore_value(input) {
//     return this.stored - 2 ** this.decompress(input).length;
//   }
// }

// const x = new BitString("0111");
// console.log(x.stored);
// console.log(x.decompress(x.stored));

// const a = 20
// const b = a.toString(2)
// console.log(b)
// const x = new BitString(b);
// console.log(x.stored)
// const c = x.restore_value(x.stored)
// console.log(c)

// example of updating
// const x = new BitString("10100");
// console.log(x.stored)
// x.stored = x.compress(x.decompress(x.stored + 1));
// console.log(x.stored)


// let s = '01010';

// for (let c of s) {
//   console.log(c);
// }

// const compress = (bits) => {
//   let sentinel = '1' + bits;
//   return parseInt(sentinel, 2);
// }

// const decompress = (input) => {
//   return input.toString(2).slice(1); 
// }

// console.log(compress('0001'));
// console.log(compress('11111111'));
// console.log(decompress(compress('0001')));

// class Item {
//   constructor(i) {
//     this.stored = i;
//   }
// }




// "use strict";

// class BitString {
//   constructor(bits) {
//     this.update(this.compress(bits));
//   }

//   update(input) {
//     if (input < 1) {
//       console.error("value error");
//     } else {
//       this.stored = input;
//     }
//   }

//   compress(bits) {
//     let sentinel = '1' + bits; // sentinel allows the bit string to retain any leading '0's
//     return parseInt(sentinel, 2);
//   }

//   decompress() {
//     return this.stored.toString(2).slice(1); // slice removes the first character of the string, which was added by the sentinel
//   }

//   value() {
//     return this.stored - 2 ** this.decompress(this.stored).length; // removes whatever was added by the sentinel
//   }

//   get_item(key) {
//     return this.decompress(this.stored)[key];
//   }

//   [Symbol.iterator]() {
//     this.index = 0;
//     return {
//       next: () => {
//         if (this.index < this.decompress().length) {
//           return {value: this.get_item(this.index++), done: false};
//         } else {
//           this.index = 0; // If you want to iterate over this again without forcing manual update of the index
//           return {done: true};
//         }
//       }
//     }
//   }
// }

// const bits = new BitString("101011011011101010011");
// // console.log(bits.get_item(2));

// for (let bit of bits) {
//   console.log(bit);
// }

import { CompressedGene } from './CompressedGene.js';

// let input = "ACGGTAGTTGTACGTATAAACGAGACACACGTAACCGTACGGTAACGTATCAGTTACGACAGACGTACGCGTAAACAACGGTACGACTACTTACGCCTACGATTTACGATCCGTGTAAGTAGACGTACACCGTAACTAGCGTATACGGTAGTTGTACGTACATCAGTTACGAGACGTACGGTACTACACCGTAACCGTAGACGTACTTACGAGACGTACGAACGTCTACGGTGTACGTATACGCTACGCGTACCGTAACTGACGTACAACGGTAGTTGTACGTATATACGGTACTACACCGTAACCGTAGACGTACGTCTAGCGCTTACGTCGCACGGCGTAGTACTACGATTTACCGTGATACGCTACGCGATACGGTACTACACGCTACGCGTACGTCCGTAACTAGCGTACGATACGGTAGTTGTAAGATACGTAGTACTACGATTTACGATACGCTACGCGATACGGAACGTCTACGGTGTACGTATACGCTACGCGATACTCAGTTACGACAGTACGACGTAGTCTAGCGCACGTCGCGTAGTACTACGAACGACGTACATCAGAGCGTACGATACGGTAGTTGTAAGATACGCACACGTAACCGACGTACGGTAACGTACGCGTAAACAACGGTACGACGACTACTTACGCCTACGATTTACGATCCGTGTAAGTCGTACGCGTACCGTAACTA"
let input = "ACGT"
const a = new CompressedGene(input)