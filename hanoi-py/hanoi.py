from typing import TypeVar, Generic, List
T = TypeVar('T')


class Stack(Generic[T]):

    def __init__(self) -> None:
        self._container: List[T] = []

    def push(self, item: T) -> None:
        self._container.append(item)

    def pop(self) -> T:
        return self._container.pop()

    def __repr__(self) -> str:
        return repr(self._container)


num_discs: int = 3
tower_a: Stack[int] = Stack()
tower_b: Stack[int] = Stack()
tower_c: Stack[int] = Stack()
tower_d: Stack[int] = Stack()
tower_e: Stack[int] = Stack()
tower_f: Stack[int] = Stack()
tower_g: Stack[int] = Stack()
tower_h: Stack[int] = Stack()
tower_i: Stack[int] = Stack()


for i in range(1, num_discs + 1):
    tower_a.push(i)

def move_second_to_end(input: tuple) -> tuple:
    tmp: list = list(input)
    tmp.append(tmp.pop(1))
    return tuple(tmp)


def move_second_to_third(input: tuple) -> tuple:
    tmp: list = list(input)
    tmp[1], tmp[2] = tmp[2], tmp[1]
    return tuple(tmp)

def move_first_to_third(input: tuple) -> tuple:
    tmp: list = list(input)
    tmp[0], tmp[2] = tmp[2], tmp[0]
    return tuple(tmp)

def third_rec_order(input: tuple) -> tuple:
    tmp: list = list(input)
    tmp.append(tmp.pop(1))
    tmp.append(tmp.pop(0))
    return tuple(tmp)

def hanoi(n: int, *towers) -> None:
    if n == 1:
        towers[1].push(towers[0].pop())
    else:
        hanoi(n - 1, *move_second_to_third(towers))
        hanoi(1, *towers)
        hanoi(n - 1, *move_first_to_third(towers))

if __name__ == "__main__":
    hanoi(num_discs, tower_a, tower_i, tower_b, tower_c, tower_d, tower_e, tower_f, tower_g, tower_h)
    print(tower_a)
    print(tower_b)
    print(tower_c)
    print(tower_d)
    print(tower_e)
    print(tower_f)
    print(tower_g)
    print(tower_h)
    print(tower_i)
