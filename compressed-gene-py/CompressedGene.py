import BitString

class CompressedGene:

    def __init__(self, gene: str) -> None:
        CompressedGene._compress(self, gene)

    def _compress(self, gene: str) -> int:
        self.bit_string = BitString.BitString("")
        for nucleotide in gene.upper():
            self.bit_string.update(self.bit_string.compressed << 2)
            if nucleotide == "A":
                self.bit_string.update(self.bit_string.compressed | 0b00)
            elif nucleotide == "C":
                self.bit_string.update(self.bit_string.compressed | 0b01)
            elif nucleotide == "G":
                self.bit_string.update(self.bit_string.compressed | 0b10)
            elif nucleotide == "T":
                self.bit_string.update(self.bit_string.compressed | 0b11)
            else:
                raise ValueError("Invalid Nucleotide:{}".format(nucleotide))

    def decompress(self) -> str:
        self.gene: str = ""
        bits = self.bit_string.iterable()
        for bit in bits:
            bit_section: str = ""
            bit_section += bit
            bit_section += bits.__next__()
            if bit_section == "00":
                self.gene += "A"
            elif bit_section == "01":
                self.gene += "C"
            elif bit_section == "10":
                self.gene += "G"
            elif bit_section == "11":
                self.gene += "T"
            else:
                raise ValueError("Invalid bits:{}".format(bit_section))
        return self.gene
