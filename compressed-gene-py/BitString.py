class BitString:

    def __init__(self, bits: str):
        BitString.update(self, BitString.compress(self, bits))

    def compress(self, bits: str) -> int:
        sentinel: int = 1 # this allows bit string to retain any leading '0's
        for bit in bits:
            sentinel <<= 1
            if bit is '0':
                sentinel |= 0b0
            elif bit is '1':
                sentinel |= 0b1
            else:
                raise ValueError("Invalid bit:{}.format(bit)")
        return sentinel

    def update(self, input: int):
        if input < 1:
            raise ValueError # A bit string should never represent a negative number, and the binary representation of the stored value always has a leading '1' in order to retain any leading '0's
        else:
            self.compressed = input # Warning, editing self.compressed directly can lead to errors. Use this update method instead of e.g. "x.compressed = 5"

    def decompress(self) -> str:
        return bin(self.compressed)[3:] # "[3:]" excludes '0b' and the sentinel value

    def restore_value(self) -> int:
        return self.compressed - (2**(self.compressed.bit_length() - 1)) # returns the value represented by the initial bit string input

    def to_bit_string(self, i: int) -> str:
        return bin(i)[2:] # returns the bit string representation of any integer

    def iterable(self) -> str:
        return iter(BitString.decompress(self))

    def __getitem__(self, key: int) -> str:
        return BitString.decompress(self)[key]