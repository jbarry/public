import CompressedGene

def test_answer():
    input: str = "ACGT"
    a = CompressedGene.CompressedGene(input)
    assert a.decompress() == input

