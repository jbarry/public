#include <vector>
#include <bitset>
#include <iostream>
#include <cmath>
#include <algorithm>

int main()
{
	std::vector<double> possibilities;
	std::vector<double> weights = {2.5,5,5,10,10,20,50,72,78,90,92};
	const int barbell = 45;
	const int number_of_weights = 10;
	for (int i=0;i<std::pow(2, number_of_weights);++i)
	{
		double sum = barbell;
		std::bitset<number_of_weights> combinations(i);
		for (int i=0;i<number_of_weights;++i)
		{
			if (combinations[i])
			{
				sum += weights[i];	
			}
		}
		possibilities.push_back(sum);
	}
	std::sort(possibilities.begin(), possibilities.end());
	auto last = std::unique(possibilities.begin(), possibilities.end());
	possibilities.erase(last, possibilities.end());
	for (double i : possibilities)
	{
		std::cout << i << '\n';
	}
}
